Vue.component('game-instance',{
	props: ['gameInstanceTiles'],
	template: 	`
				<div class='game-instance'>
					<game-tile v-for='gameTile in gameInstanceTiles' v-bind:gameTile=gameTile v-bind:gameInstanceTiles=gameInstanceTiles></game-tile>
				</div>
				`,
	methods: {
		initializeGameTile: function(tileID){
			var tile = 	{	
							id: tileID,
							state: true
						}
			return tile;
		},
		initializeGameTiles: function(){
			for (var tileNumber = 0; tileNumber < 9; tileNumber++){
				var tileInstance = this.initializeGameTile(tileNumber);
				this.gameInstanceTiles.push(tileInstance);
			}
		},
		randomNumberBetween: function(min, max){
			return Math.floor(Math.random() * (max - min))
		},
		randomizeInstance: function(level){
			if (level > 0){
				var flipCount = 0;
				var flipped = [];
				for (flipCount; flipCount < level; flipCount++){
					var randomTile = this.randomNumberBetween(0,9);
					this.$children[randomTile].flipStateX_Cross(randomTile);
					flipped.push(randomTile);
				}
				console.log("Proposed Solution: "+flipped);
				console.log("Minimized Solution: "+this.minimizeSolution(flipped));
			}else{
				console.log("Level must be a positive whole number");
			}
		},
		minimizeSolution: function(proposedSolution){
			minimizedSolution = proposedSolution;
			var index = 0;
			var nextIndex;
			console.log("started")
			while (index < minimizedSolution.length - 1){
				/*
				for (nextIndex = index+1; nextIndex < minimizedSolution.length; nextIndex++){
					console.log("Index: "+index+" NextIndex: "+nextIndex);
					if (minimizedSolution[index] == minimizedSolution[nextIndex]){
						minimizedSolution.splice(nextIndex, 1);
						minimizedSolution.splice(index, 1);
						console.log("spliced: "+index+" and "+nextIndex);
						//break;
						nextIndex = index;
					}else{
						console.log("no splice: " + index);
					}
				}
				index++;
				*/
				
				nextIndex = index+1;
				while (nextIndex < minimizedSolution.length){
					if (minimizedSolution[index] == minimizedSolution[nextIndex]){
						minimizedSolution.splice(nextIndex, 1);
						minimizedSolution.splice(index, 1);
						console.log("spliced: "+index+" and "+nextIndex);
						nextIndex = index+1;
					}else{
						console.log("no splice: "+index);
						nextIndex++;
					}
				}
				index++;
				console.log(index);
			}
			console.log("ended");
			return minimizedSolution;
		}
	},
	created: function(){
		this.initializeGameTiles();
	},
	mounted: function(){
		this.randomizeInstance(9);
	}
	
})

Vue.component('game-tile',{
	props: ['gameTile','gameInstanceTiles'],
	template: 	`
				<div class='game-tile' v-on:click='flipStateX_Cross(gameTile.id)' v-bind:class="{'lit':gameTile.state,'dim':!gameTile.state}">{{gameTile.id}}</div>
				`,
	methods: {
		flipState: function(id){
			//console.log(this.gameInstanceTiles[id]);
			this.gameInstanceTiles[id].state = !this.gameInstanceTiles[id].state;
		},
		flipStateAbove: function(id){
			var stateAbove = id-3;
			if (stateAbove >= 0){
				this.flipState(stateAbove);
			}
		},
		flipStateBelow: function(id){
			var stateBelow = id+3;
			if (stateBelow <= 8){
				this.flipState(stateBelow);
			}
		},
		flipStateLeft: function(id){
			var stateLeft = id-1;
			if (stateLeft%3 != 2 && stateLeft >= 0){
				this.flipState(stateLeft);
			}
		},
		flipStateRight: function(id){
			var stateRight = id+1;
			if (stateRight%3 != 0){
				this.flipState(stateRight);
			}
		},
		flipStateDiag_up_left: function(id){
			var stateUpLeft = (id - 1) - 3;
			if (stateUpLeft >= 0 && stateUpLeft%3 < 2){
				this.flipState(stateUpLeft);
			}
		},
		flipStateDiag_up_right: function(id){
			var stateUpRight = (id + 1) - 3;
			if (stateUpRight > 0 && stateUpRight%3 > 0){
				this.flipState(stateUpRight);
			}
		},
		flipStateDiag_low_left: function(id){
			var stateLowLeft = (id - 1) + 3;
			if (stateLowLeft <= 8 && stateLowLeft%3 != 2){
				this.flipState(stateLowLeft);
			}
		},
		flipStateDiag_low_right: function(id){
			var stateLowRight = (id + 1) + 3;
			if (stateLowRight <= 8 && stateLowRight%3 > 0){
				this.flipState(stateLowRight);
			}
		},
		flipStateT_Cross: function(id){
			this.flipState(id);
			this.flipStateAbove(id);
			this.flipStateBelow(id);
			this.flipStateRight(id);
			this.flipStateLeft(id);
		},
		flipStateX_Cross: function(id){
			this.flipState(id);
			this.flipStateDiag_up_left(id);
			this.flipStateDiag_up_right(id);
			this.flipStateDiag_low_left(id);
			this.flipStateDiag_low_right(id);
		}
	}
})

new Vue({
	el: '#vue-app',
	template: 	`
				<game-instance v-bind:gameInstanceTiles=gameInstanceTiles></game-instance>
				`, 
	data: {
		gameInstanceTiles: [],
	}
})